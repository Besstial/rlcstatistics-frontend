module "cloud_run" {
  source  = "GoogleCloudPlatform/cloud-run/google"
  version = "~> 0.3.0"

  # Required variables
  service_name          = var.service_name
  project_id            = "rlcstatistics"
  location              = "europe-west4"
  image                 = var.docker_image

  # Optional variables
  ports                  = { "name" : "http1", "port" : 5173 }
  generate_revision_name = true
  template_annotations   = {
    "autoscaling.knative.dev/minScale" : 0,
    "autoscaling.knative.dev/maxScale" : 1,
    "generated-by" : "terraform",
    "run.googleapis.com/client-name" : "terraform"
  }

    service_annotations = {
        "run.googleapis.com/ingress": "all"
    }

  service_account_email = "rlcstatistics@appspot.gserviceaccount.com"
  timeout_seconds = 300
  container_concurrency = 80

  env_vars = [
        {
          value = var.env
          name  = "VITE_ENV"
        }
    ]
}

resource "google_cloud_run_service_iam_binding" "auth" {
  location    = module.cloud_run.location
  project     = module.cloud_run.project_id
  service     = module.cloud_run.service_name
  role     = "roles/run.invoker"
  members = [
    "allUsers"
  ]
}
