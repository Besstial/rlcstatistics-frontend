variable "docker_image" {
  description = "The path to the Docker image."
  type        = string
}

variable "service_name" {
  description = "The service name."
  type        = string
}

variable "env" {
  description = "The environnement."
  type        = string
  default     = "DEV"
}