interface AppConfig {
  api_protocol: string
  api_sub_domain: string
  api_domain: string
  api_extension: string
}

interface AppConfigLocal {
  api_protocol: string
  api_domain: string
  api_port: string
}
  
const config_prd: AppConfig = {
  api_protocol: import.meta.env.VITE_API_PROTOCOL || 'https',
  api_sub_domain: import.meta.env.VITE_API_SUB_DOMAIN || 'rlcstatistics-api-prod-tdboeevhfq-ez',
  api_domain: import.meta.env.VITE_API_DOMAIN || 'a.run',
  api_extension: import.meta.env.VITE_API_EXTENSION || 'app'
}

const config_dev: AppConfig = {
  api_protocol: import.meta.env.VITE_API_PROTOCOL || 'https',
  api_sub_domain: import.meta.env.VITE_API_SUB_DOMAIN || 'rlcstatistics-api-dev-tdboeevhfq-ez',
  api_domain: import.meta.env.VITE_API_DOMAIN || 'a.run',
  api_extension: import.meta.env.VITE_API_EXTENSION || 'app'
}

const config_local: AppConfigLocal = {
  api_protocol: import.meta.env.VITE_API_PROTOCOL || 'http',
  api_domain: import.meta.env.VITE_API_DOMAIN || 'localhost',
  api_port: import.meta.env.VITE_API_PORT || '8000',
}


const api_url_prd = config_prd.api_protocol + '://' + config_prd.api_sub_domain + '.' + config_prd.api_domain + '.' + config_prd.api_extension
const api_url_dev = config_dev.api_protocol + '://' + config_dev.api_sub_domain + '.' + config_dev.api_domain + '.' + config_dev.api_extension
const api_url_local = config_local.api_protocol + '://' + config_local.api_domain + ':' + config_local.api_port

const api_url = import.meta.env.VITE_ENV === 'PRD' ? api_url_prd : import.meta.env.VITE_ENV === 'DEV' ? api_url_dev : api_url_local

export default api_url