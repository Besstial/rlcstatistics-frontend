import React from 'react'

const ApodEnsaiPrivacy: React.FC = () => {
  return (
    <div className="flex flex-col m-4 md:mx-80 space-y-2 text-justify">
      <h1 className="text-4xl font-bold">Politique de Confidentialité</h1>
      <h2 className="text-2xl font-bold">Introduction</h2>
      <p className="">Bienvenue sur APODENSAI. Nous respectons et protégeons votre vie privée. Cette politique de confidentialité explique comment nous traitons vos informations lorsque vous utilisez notre application. Nous nous engageons à garantir la confidentialité de toutes vos informations personnelles.</p>
      <h2 className="text-2xl font-bold">Absence de Collecte de Données Utilisateurs</h2>
      <p className="">APODENSAI ne collecte aucune donnée utilisateur. Nous croyons fermement que votre vie privée est un droit fondamental et nous n'avons aucun besoin d'accéder à vos informations personnelles pour vous fournir nos services. En conséquence :</p>
      <ul className="">
          <li><strong className="">Pas de collecte de données personnelles</strong> : Nous ne collectons, ne stockons ni ne partageons aucune information personnelle ou sensible.</li>
          <li><strong className="">Pas de comptes utilisateurs</strong> : APODENSAI ne nécessite pas la création de comptes utilisateurs. Vous pouvez utiliser notre application sans fournir d'informations d'identification.</li>
      </ul>
      <h2 className="text-2xl font-bold">Sécurité des Données</h2>
      <p className="">Même si nous ne collectons aucune donnée utilisateur, nous nous assurons que notre application est sécurisée et respectueuse de votre vie privée :</p>
      <ul className="">
          <li><strong className="">Sécurité de l'application</strong> : Nous mettons en œuvre des mesures de sécurité appropriées pour protéger notre application contre tout accès non autorisé ou toute modification non autorisée.</li>
          <li><strong className="">Mises à jour régulières</strong> : Nous maintenons notre application à jour pour garantir la sécurité et l'intégrité de notre service.</li>
      </ul>
      <h2 className="text-2xl font-bold">Modifications de cette Politique de Confidentialité</h2>
      <p className="">Nous nous réservons le droit de modifier cette politique de confidentialité à tout moment. Toute modification sera affichée sur cette page. Nous vous encourageons à consulter cette page régulièrement pour rester informé des mises à jour.</p>
      <h2 className="text-2xl font-bold">Contact</h2>
      <p className="">Pour toute question ou préoccupation concernant cette politique de confidentialité, n'hésitez pas à nous contacter à adrien.lacaille@gmail.com.</p>
      <p className=""><strong>Version en date du 22-06-2024</strong></p>
    </div>
  );
};

export default ApodEnsaiPrivacy;