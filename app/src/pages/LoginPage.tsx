import { useRecoilValue } from 'recoil'
import { tokenState } from "../types/recoil_state"
import UserProfile from "../components/UserProfile"
import LoginRegisterForm from "../components/LoginRegisterForm"

const LoginPage: React.FC = () => {
  
  const token = useRecoilValue(tokenState)

  return (
    <div>
      {
        token.isAuthenticated && <UserProfile />
      }
      {
        !token.isAuthenticated && <LoginRegisterForm />  
      }
    </div>
  )
}

export default LoginPage;
