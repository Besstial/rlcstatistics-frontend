import React, { useState, useEffect} from 'react'
import TeamSumUp from '../components/TeamSumUp'
import BetSelection from '../components/BetSelection'
import axios from 'axios'
import { useLocation } from "react-router-dom"
import api_url from '../config'
import { UpcomingMatchInfo } from '../types/Match'
import { convert_TeamDataApi_to_CoreStats } from '../functions/convertObjects'
import OddsComponent from '../components/OddsComponent'


const NextMatchPage: React.FC = () => {
    const id_match_route = useLocation().pathname.split('/').pop()
    const [upcomingMatchData, setUpcomingMatchData] = useState<UpcomingMatchInfo>()
  
    useEffect(() => {
        fetchUpcomingMatchData()
    }, [id_match_route])
  
    async function fetchUpcomingMatchData() {
        try {
            const response = await axios.get<UpcomingMatchInfo>(
                `${api_url}/upcoming_match/${id_match_route}`, {
                headers: {
                    'Content-Type': 'application/json'
                }
            })
            setUpcomingMatchData(response.data)
        } catch (error) {
            console.error(error)
        }   
    }

    console.log(upcomingMatchData)

    const orange_team_name = upcomingMatchData?.orange.name ? upcomingMatchData.orange.name : "Team Orange"
    const blue_team_name = upcomingMatchData?.blue.name ? upcomingMatchData.blue.name : "Team Blue"
    const orange_team_core = upcomingMatchData?.orange ? convert_TeamDataApi_to_CoreStats(upcomingMatchData.orange) : {}
    const blue_team_core = upcomingMatchData?.blue ? convert_TeamDataApi_to_CoreStats(upcomingMatchData.blue)  : {}

    console.log(orange_team_name)
  return (
    <div>
        { upcomingMatchData && 
            <div className="flex flex-col sm:flex-row items-center justify-around lg:space-x-4 sm: space-y-4">
                <TeamSumUp name={orange_team_name} core={orange_team_core} color="orange" />
                <div>
                    <BetSelection orange_team_name={orange_team_name} blue_team_name={blue_team_name} /> 
                    <OddsComponent blue_team_name={blue_team_name} blue_odd={upcomingMatchData.blue_odd} orange_team_name={orange_team_name} orange_odd={upcomingMatchData.orange_odd} />
                </div>
                <TeamSumUp name={blue_team_name} core={blue_team_core} color="blue" />
            </div>
        }
    </div>
  )
}

export default NextMatchPage
