import axios from "axios"
import React, { useEffect, useState } from "react"
import api_url from "../config"
import LeaderboardTable from "../components/LeaderboardTable"


const LeaderboardPage: React.FC = () => {

    const [ userLeaderboard, setUserLeaderboard ] = useState<UserLeaderboard[]>([])

    useEffect(() => {
      fetchData()
    }, [])
  
    async function fetchData() {
      try {
        const response = await axios.get<UserLeaderboard[]>(`${api_url}/leaderboard`)
        setUserLeaderboard(response.data)

      } catch (error) {
        console.error(error)
      }
    }

  
    return (
      <div className="container mx-auto">
        <div className="flex flex-col items-center">
          <h1 className="text-3xl font-bold mb-4">Leaderboard</h1>
          <LeaderboardTable list={userLeaderboard} />
        </div>
      </div>
    )
  }

export default LeaderboardPage