import React from 'react'
import CancelButton from "../components/CancelButton"

const ErrorPage: React.FC = () => {
  return (
    <div className="flex flex-col items-center justify-center min-h-screen bg-gray-100">
      <h1 className="text-4xl font-bold text-gray-900">Erreur 404</h1>
      <p className="mt-4 my-4 text-lg text-gray-600">
        Failed, this page doesn't exist.
      </p>
      <CancelButton />
    </div>
  );
};

export default ErrorPage;