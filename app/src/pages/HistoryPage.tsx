import axios from "axios"
import React, { useEffect, useState } from "react"
import { Outlet, useNavigate } from "react-router-dom"
import api_url from "../config"
import { useRecoilState } from 'recoil'
import { tokenState, tokenStateInit } from "../types/recoil_state"
import { BetMatch } from "../types/Match"
import BetTable from "../components/BetTable"
import Pagination from "../components/Pagination"


const HistoryPage: React.FC = () => {

    const navigate = useNavigate()
    const [bets, setBets] = useState<BetMatch[]>([])
    const [token, setToken] = useRecoilState(tokenState)
    const [currentPage, setCurrentPage] = useState<number>(1)
    const pageSize = 8

  
    useEffect(() => {
      fetchData()
    }, [])
  
    async function fetchData() {
      try {
        const response = await axios.get<BetMatch[]>(`${api_url}/bets/me`, {
          headers: {
            'Content-Type': 'application/json',
            'Authorization': `${token.token}`
          }
        })
        const data = response.data
        setBets(data)
      } catch (error) {
        console.error(error)
        setToken(tokenStateInit)
        navigate("/login")
      }
    }

    const handlePageChange = (newPage: number) => {setCurrentPage(newPage)}
  
    return (
      <div className="flex justify-evenly flex-col sm:flex-row items-center space-y-8">
        <div className="flex-col">
          <h1 className="text-3xl font-bold mb-4 text-center">Bets</h1>
          <BetTable bets={bets.slice((currentPage - 1) * pageSize, currentPage * pageSize)} />
          <Pagination currentPage={currentPage} pageSize={pageSize} totalItems={bets.length} onPageChange={handlePageChange} />
        </div>
        <Outlet />
      </div>
    )
  }

export default HistoryPage