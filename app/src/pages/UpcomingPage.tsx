import React, { useState, useEffect } from "react"
import axios from "axios"
import api_url from "../config"
import { Outlet } from "react-router-dom"
import { UpcomingMatch } from "../types/Match"
import UpcomingMatchTable from "../components/UpcomingMatchTable"
import Pagination from "../components/Pagination"


const UpcomingPage: React.FC = () => {
  const [upcomingMatches, setUpcomingMatches] = useState<UpcomingMatch[]>([])
  const [currentPage, setCurrentPage] = useState<number>(1)
  const pageSize = 8

  useEffect(() => {
    fetchData()
  }, [])

  async function fetchData() {
    try {
      const response = await axios.get(`${api_url}/upcoming_matches`, {
        headers: {
          'Content-Type': 'application/json'
        }
      })

      const data = response.data
      setUpcomingMatches(data)
    } catch (error) {
      console.error(error)
    }
  }

  const handlePageChange = (newPage: number) => {setCurrentPage(newPage)}

  return (
    <div className="flex justify-evenly flex-col sm:flex-row items-center space-y-8">
      <div className="flex-col">
        <h1 className="text-3xl font-bold mb-4 text-center">Upcoming Matches</h1>
        <UpcomingMatchTable upcoming_matches={upcomingMatches.slice((currentPage - 1) * pageSize, currentPage * pageSize)} />
        <Pagination currentPage={currentPage} pageSize={pageSize} totalItems={upcomingMatches.length} onPageChange={handlePageChange} />
      </div>
        <Outlet />
    </div>
  )
}

export default UpcomingPage;