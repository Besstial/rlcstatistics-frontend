import React, { useState, useEffect} from 'react'
import { useLocation } from "react-router-dom"
import axios from 'axios'
import api_url from '../config'
import ScoreComponent from '../components/ScoreComponent'
import { MatchData } from '../types/Match'
import TeamSumUp from '../components/TeamSumUp'


const MatchPage: React.FC = () => {

  const id_match_route = useLocation().pathname.split('/').pop()
  const [matchData, setMatchData] = useState<MatchData>()

  useEffect(() => {
      fetchMatchData()
  }, [id_match_route])

  async function fetchMatchData() {
      try {
          const response = await axios.get<MatchData>(
              `${api_url}/match/${id_match_route}`, {
              headers: {
                  'Content-Type': 'application/json'
              }
          })
          setMatchData(response.data)
      } catch (error) {
        setMatchData(undefined)
      }   
  }

  const orange_team_name = matchData?.orange.team.team ? matchData.orange.team.team.name : "Team Orange"
  const blue_team_name = matchData?.blue.team.team ? matchData.blue.team.team.name : "Team Blue"
  const orange_team_core = matchData?.orange.team.stats ? matchData?.orange.team.stats.core : {}
  const blue_team_core = matchData?.blue.team.stats ? matchData?.blue.team.stats.core : {}

  return (
    <div>
      {
        !matchData &&
        <p className='font-bold'>Match not yet updated</p>
      }
      { matchData && 
        <div className="flex flex-col sm:flex-row items-center justify-around lg:space-x-4 sm: space-y-4">
          <TeamSumUp name={orange_team_name} core={orange_team_core} color="orange" />
          <div className='px-8'>
            <ScoreComponent blue={matchData.blue.score} orange={matchData.orange.score} />
          </div>
          <TeamSumUp name={blue_team_name} core={blue_team_core} color="blue" />
        </div>
      }
    </div>
  )
}

export default MatchPage;
