import React, { useState, useEffect } from 'react'
import SearchBar from '../components/SearchBar'
import axios from 'axios'
import api_url from '../config'
import IndexMatchTable from '../components/IndexMatchTable';
import { IndexMatch } from '../types/Match';


const SearchPage: React.FC = () => {
  const [results, setResults] = useState<IndexMatch[]>([]);
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState<string | null>(null);
  const [page, setPage] = useState(1);
  const [totalCount, setTotalCount] = useState(0);
  const pageSize = 10;


  useEffect(() => {
    const storedResults = localStorage.getItem('searchResults');
    const storedPage = localStorage.getItem('searchPage');
    const storedTotalCount = localStorage.getItem('searchTotalCount');

    if (storedResults && storedPage && storedTotalCount) {
      setResults(JSON.parse(storedResults));
      setPage(Number(storedPage));
      setTotalCount(Number(storedTotalCount));
    }
  }, []);


  const handleSearch = async (searchType: string, query: string, page: number) => {
    setLoading(true);
    setError(null);

    try {
      const response = await axios.get(`${api_url}/matches`, {
        params: {
          [searchType.toLowerCase()]: query,
          page: page,
          page_size: pageSize,
        },
      });

      setResults(response.data.matches);
      setTotalCount(response.data.totalCount);

      localStorage.setItem('searchResults', JSON.stringify(response.data.matches));
      localStorage.setItem('searchPage', String(page));
      localStorage.setItem('searchTotalCount', String(response.data.totalCount));
    } catch (err) {
      setError('Erreur lors de la recherche.');
    } finally {
      setLoading(false);
    }
  };

  const handlePageChange = (newPage: number) => {
    setPage(newPage);
    handleSearch('Tous', '', newPage);
  };

  const totalPages = Math.ceil(totalCount / pageSize);

  return (
    <div className="p-4">
      <SearchBar onSearch={(searchType, query) => handleSearch(searchType, query, 1)} />
      {loading && <p>Loading...</p>}
      {error && <p className="text-red-500">{error}</p>}

      <div className="mt-6">
        {results.length > 0 ? (
          <>
            <IndexMatchTable matches={results} />

            {/* Pagination */}
            <div className="flex justify-center mt-4">
              <button 
                disabled={page === 1} 
                onClick={() => handlePageChange(page - 1)} 
                className="mx-2 px-4 py-2 bg-blue-500 text-white rounded disabled:opacity-50"
              >
                Précédent
              </button>
              <span className="mx-4">Page {page} sur {totalPages}</span>
              <button 
                disabled={page === totalPages} 
                onClick={() => handlePageChange(page + 1)} 
                className="mx-2 px-4 py-2 bg-blue-500 text-white rounded disabled:opacity-50"
              >
                Suivant
              </button>
            </div>
          </>
        ) : (
          !loading && <p>No results found</p>
        )}
      </div>
    </div>
  );
};

export default SearchPage;
