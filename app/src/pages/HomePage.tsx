import React from "react"
import { Outlet } from "react-router-dom"
import Header from "../components/Header"
import Footer from "../components/Footer"

const HomePage: React.FC = () => {
  return (
    <div className="flex flex-col">
      <Header />
      <main className="py-3">
        <Outlet />
      </main>
      <Footer />
    </div>
  );
};

export default HomePage;