import axios, { AxiosError } from 'axios'
import api_url from "../config"


async function signup(email: string, username: string, password: string): Promise<[boolean, string]> {
  
  try {
    await axios.post<User>(`${api_url}/user`, 
      {
        email: email,
        username: username,
        password: password
      },
      {
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded'
        }
      }
    )
    return [ true, "Successfull registration" ]
  } catch (error) {
    const axiosError = error as AxiosError
    console.error(axiosError.response?.data)
    return [ false, axiosError.response?.data.detail ] 
  }
}

export default signup;
