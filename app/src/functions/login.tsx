import axios, { AxiosError } from 'axios'
import api_url from "../config"
import { tokenStateInit, userStateInit } from '../types/recoil_state'


async function login(email: string, password: string): Promise<[Token, User, string]> {
  
  try {
    const response = await axios.post<LoginApi>(`${api_url}/login`, 
      {
        grant_type: null,
        username: email,
        password: password,
        scope: "me",
        client_id: null,
        client_secret: null
      },
      {
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded'
        }
      }
    )

    const token: Token = {
      isAuthenticated: true,
      token: `${response.data.token_type} ${response.data.access_token}`
    }

    const user = await axios.get<User>(`${api_url}/user/me`,
      {
        headers: {
          'Content-Type': 'application/json',
          'Authorization': `${token.token}`
        }
      }
    )

    return [token, user.data, "Successful login."]
  } catch (error) {
    const axiosError = error as AxiosError
    console.log(error)
    console.error(axiosError.response?.data)
    return [ tokenStateInit, userStateInit, axiosError.response?.data.detail] 
  }
}

export default login;
