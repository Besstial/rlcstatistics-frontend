import { CoreStats, TeamDataApi } from '../types/Match';


export function convert_TeamDataApi_to_CoreStats(object: TeamDataApi): CoreStats {
  const result: CoreStats = {
    score: object.score,
    goals: object.goals,
    shots: object.shots,
    assists: object.assists,
    saves: object.saves
  }
  return result
}

export function transformBetsForCharts(bet_data: Record<string, Bet>): [LineChartData[], PieChartData[]] {
  return [ tranformBetsForLineChart(bet_data), tranformBetsForPieChart(bet_data) ]
}

function tranformBetsForLineChart(bet_data: Record<string, Bet>): LineChartData[] {
  const result: LineChartData[] = []

  let cumulativeSum = 100;
  for (const betId in bet_data) {
    if (bet_data.hasOwnProperty(betId)) {
      const bet = bet_data[betId];
      cumulativeSum += (bet.reward || 0) - bet.amount;
      result.push({ date: bet.date, coins: cumulativeSum });
    }
  }
  return result
}

function tranformBetsForPieChart(bet_data: Record<string, Bet>): PieChartData[] {
  let successfulCount = 0;
  let unsuccessfulCount = 0;

  for (const betId in bet_data) {
    if (bet_data.hasOwnProperty(betId)) {
      const bet = bet_data[betId];
      if (bet.reward !== null && bet.reward > 0) {
        successfulCount++;
      } else {
        unsuccessfulCount++;
      }
    }
  }

  const result: PieChartData[] = [
    { name: "Loose", value: unsuccessfulCount },
    { name: "Win", value: successfulCount },
  ];

  return result;
}
