import axios from "axios"
import api_url from "../config"

async function give_coins(token: string) {
    try {
        await axios.get(`${api_url}/refill/me`, 
            {
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `${token}`
                }
            }
        )
    } catch (error) {
        console.error(error);
    }
}

export default give_coins;