import axios, { AxiosError } from 'axios'
import api_url from "../config"
import { userStateInit } from '../types/recoil_state';


async function get_user(token: string): Promise<User> {
    try {
      const user = await axios.get<User>(`${api_url}/user/me`,
        {
          headers: {
            'Content-Type': 'application/json',
            'Authorization': `${token}`
          }
        }
      )
      return user.data
    } catch (error) {
      const axiosError = error as AxiosError
      console.error(axiosError.response?.data)
      return userStateInit
    }   
}

export default get_user;
