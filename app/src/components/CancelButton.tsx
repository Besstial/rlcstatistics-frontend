import React from 'react'
import { useNavigate  } from "react-router-dom"

const LoginButton: React.FC = () => {
    const navigate = useNavigate()
    return (
        <div>
            <button 
                className="bg-red-500 hover:bg-gray-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline" 
                type="button" 
                onClick={() => navigate(-1)}>
                    Cancel
            </button>
        </div>
    )
}

export default LoginButton;

