import React from 'react'
import { useNavigate } from 'react-router-dom'

const Profile: React.FC = () => {

  const navigate = useNavigate()

  return (
    <div>
        <button className="flex items-center px-3 py-2 rounded text-gray-600 hover:text-gray-800" onClick={() => navigate("/login")}>
            <img src="/src/assets/user-profile-account-person.svg" alt="Generic Profile" className="h-8 w-8 rounded-full" />
        </button>
    </div>
  )
}

export default Profile;
