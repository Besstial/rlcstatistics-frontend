import { useState } from "react"
import login from "../functions/login"
import { useNavigate } from "react-router-dom"
import { useSetRecoilState } from 'recoil'
import { tokenState, userState } from "../types/recoil_state"

interface LoginFormProps {
  isActive: boolean
  onShow: () => void
}

const LoginForm: React.FC<LoginFormProps> = ( props ) => {
  const navigate = useNavigate()
  
  const setToken = useSetRecoilState(tokenState)
  const setUser = useSetRecoilState(userState)
  
  const [email, setEmail] = useState("")
  const handleEmailChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setEmail(event.target.value)
  }

  const [password, setPassword] = useState("")
  const handlePasswordChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setPassword(event.target.value)
  }

  const [errorLoginMessage, setErrorLoginMessage] = useState("")
  const handleLoginSubmit = async (e: React.FormEvent) => {
    e.preventDefault()

    try {
      const [token, user, loginMessage] = await login(email, password)
      setToken(token);
      setUser(user);
      setErrorLoginMessage(loginMessage);

      if (token.isAuthenticated) {
        navigate(-1);
      }
    } catch (error) {
      console.error("An error occurred during login:", error);
      setErrorLoginMessage("An error occurred during login. Please try again.");
    }
  }

  return (
    <div className="flex justify-center">
      {
        props.isActive && 
          <div className="p-4 rounded shadow-md">
            <h2 className="text-xl font-bold mb-4">Log In</h2>
            <form onSubmit={handleLoginSubmit}>
              <div className="mb-4">
                <label className="text-gray-700 font-bold" htmlFor="email">Email</label>
                <input
                  className="border rounded w-full p-2 text-gray-700 focus:outline-none"
                  type="email"
                  name="email"
                  placeholder="myemail@smtp.com"
                  value={email}
                  onChange={handleEmailChange}
                />
              </div>
              <div className="mb-4">
                <label className="text-gray-700 font-bold" htmlFor="password">Password</label>
                <input
                  className="border rounded w-full p-2 text-gray-700 focus:outline-none"
                  type="password"
                  name="password"
                  placeholder="**********"
                  value={password}
                  onChange={handlePasswordChange}
                />
              </div>
              <div className="flex items-center justify-between pb-4">
                <button className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded" type="submit" disabled={!email || !password}>Sign In</button>
                <button className="bg-green-500 hover:bg-green-700 text-white font-bold py-2 px-4 rounded" type="button" onClick={props.onShow}>Register</button>
              </div>
              {
                errorLoginMessage &&
                  <div className="bg-red-100 border border-red-400 text-red-700 px-4 py-2 rounded" role="alert">
                    <strong className="font-bold">{errorLoginMessage}</strong>
                  </div>
              }
            </form>
          </div>
      }
    </div>
  )
}

export default LoginForm;
