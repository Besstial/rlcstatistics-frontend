import { useState } from "react";
import LoginForm from "./LoginForm";
import RegisterForm from "./RegisterForm";

const LoginRegisterForm: React.FC = () => {
    
    const [showRegistration, setShowRegistration] = useState(true)

    return (
        <div>
            <LoginForm isActive={showRegistration} onShow={() => setShowRegistration(!showRegistration)} />
            <RegisterForm isActive={!showRegistration} onShow={() => setShowRegistration(!showRegistration)} />
        </div>
    )
}

export default LoginRegisterForm;
