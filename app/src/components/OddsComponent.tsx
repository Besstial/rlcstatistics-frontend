import React from 'react'
import OddComponent from './OddComponent'

interface OddsComponentProps {
    blue_team_name: string
    blue_odd: number
    orange_team_name: string
    orange_odd: number
}

const OddsComponent: React.FC<OddsComponentProps> = ( props ) => {

  return (
    <div>
      <p className='text-xl text-center font-bold py-1'>Odds</p>
      <div className='flex flex-row justify-around'>
        <OddComponent team_name={props.orange_team_name} odd={props.orange_odd} color='orange' />
        <OddComponent team_name={props.blue_team_name} odd={props.blue_odd} color='blue' />
      </div>
    </div>
    
  )
}

export default OddsComponent;