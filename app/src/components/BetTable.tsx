import React from "react"
import { useNavigate } from "react-router-dom"
import { BetMatch } from "../types/Match"


interface BetTableProps {
    bets: BetMatch[]
}

const BetTable: React.FC<BetTableProps> = ({ bets }) => {
  
    const betArray = Object.values(bets);
    const navigate = useNavigate()
    const handleRowClick = (matchId: string) => {
      const matchUrl = `match/${matchId}`
      navigate(matchUrl)
    }
    
    return (
        <table className="table-auto bg-white shadow-lg rounded-lg overflow-hidden">
          <thead>
            <tr className="bg-gray-200 text-gray-600 uppercase text-sm leading-normal">
              <th className="py-3 px-4 text-left">Match</th>
              <th className="py-3 px-4 text-left">Date</th>
              <th className="py-3 px-4 text-left">Bet</th>
              <th className="py-3 px-4 text-left">Reward</th>
            </tr>
          </thead>
          <tbody>
            {betArray.map((bet, index) => {
              const formattedDate = new Date(bet.date).toLocaleString([], { year: 'numeric', month: '2-digit', day: '2-digit', hour: '2-digit', minute: '2-digit' })
  
              return (
                <tr
                  key={bet.uuid}
                  className={`${index % 2 === 0 ? "" : "bg-gray-100"} hover:bg-gray-300 cursor-pointer`}
                  onClick={() => handleRowClick(bet.id_match)}
                >
                  <td className="py-3 px-4">
                    <div className="flex items-center">
                      <div>
                        <p className={`${bet.reward !== undefined && bet.reward > 0 ? "font-semibold": "text-sm text-gray-500"}`}>{bet.team_name}</p>
                      </div>
                    </div>
                  </td>
                  <td className="py-3 px-4">{formattedDate}</td>
                  <td className="py-3 px-4">{bet.amount}</td>
                  <td className="py-3 px-4">{bet.reward}</td>
                </tr>
              );
            })}
          </tbody>
        </table>
    )
  }

  export default BetTable