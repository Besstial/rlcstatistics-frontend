import { useRecoilState, useSetRecoilState } from 'recoil'
import { tokenState, tokenStateInit, userState, userStateInit } from "../types/recoil_state"

const UserProfile: React.FC = () => {

    const setToken = useSetRecoilState(tokenState)
    const [user, setUser] = useRecoilState(userState)

    const resetUser = () => {
        setToken(tokenStateInit)
        setUser(userStateInit)
    }

    return (
        <div className="flex justify-around">
            <div className="flex flex-col items-center justify-around">
                <p>Hi {user.username} !</p>
                <p>You have {user.coins} coins.</p>
            </div>
            <button className="bg-red-500 hover:bg-red-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline" type="button" onClick={() => resetUser()}>Log Out</button>
        </div>
    )
}

export default UserProfile;
