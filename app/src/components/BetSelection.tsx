import React, { useEffect, useState } from 'react'
import axios, { AxiosError } from 'axios'
import api_url from '../config'
import { useRecoilState } from 'recoil'
import { tokenState, tokenStateInit, userState } from '../types/recoil_state'
import { useLocation } from 'react-router-dom'

interface BetSelectionProps {
  orange_team_name: string
  blue_team_name: string
}

const BetSelection: React.FC<BetSelectionProps> = ( props ) => {
  
  const id_match_route = useLocation().pathname.split('/').pop()
  const [selectedTeam, setSelectedTeam] = useState<string>(props.orange_team_name)
  const [betAmount, setBetAmount] = useState<number>(0)
  const [token, setToken] = useRecoilState(tokenState)
  const [user, setUser] = useRecoilState(userState)
  const [hasBet, setHasBet] = useState<boolean>(false)
  const [hasError, setHasError] = useState<boolean>(false)
  const [errorMessage, setErrorMessage] = useState<string>('No bet register')

  useEffect(() => {
    setSelectedTeam(props.orange_team_name)
  }, [props.orange_team_name])

  const handleTeamSelection = (event: React.ChangeEvent<HTMLSelectElement>) => {
    setSelectedTeam(event.target.value)
  }

  const handleBetAmountChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const value = parseInt(event.target.value)
    if (value < 10000) {
      setBetAmount(value)
    } else if (value >= 10000 ) {
      setBetAmount(9999)
    } else {
      setBetAmount(0)
    }
  }

  async function handleBet() {
    try {
      await axios.post(
        `${api_url}/bet`,
        {
          id_match: id_match_route,
          team_name: selectedTeam,
          amount: betAmount
        },
        {
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
            'Authorization': `${token.token}`
          }
        }
      )
      setHasBet(true)
      const updatedUser = { ...user, coins: user.coins - betAmount }
      setUser(updatedUser)
      setHasError(false)
    } catch (error) {
      const axiosError = error as AxiosError
      if (axiosError.response?.status == 401) {
        setHasBet(false)
        setErrorMessage(axiosError.response?.data.detail)    
        setHasError(true)    
        setToken(tokenStateInit)
      } else {
        setHasBet(false)
        setErrorMessage(axiosError.response?.data.detail)
        setHasError(true)
      }
    } finally {
      setBetAmount(0)
    }
  }

  return (
    <div className="flex flex-col space-y-2 bg-gray-100 rounded-md px-4 py-2">
      <h2 className="text-xl font-semibold">Choose a team to bet on:</h2>
      <select
        value={selectedTeam}
        onChange={(event) => handleTeamSelection(event)}
        className="w-full py-2 px-4 border border-gray-300 rounded-md focus:outline-none focus:border-blue-500">
        <option value={props.orange_team_name}>{props.orange_team_name}</option>
        <option value={props.blue_team_name}>{props.blue_team_name}</option>
      </select>
      <label htmlFor="betAmount" className="font-medium">Bet amount:</label>
      <input type="number" id="betAmount" value={betAmount.toString()} step={10} min={0} max={10000}
        onChange={(event) => handleBetAmountChange(event)}
        className="w-full py-2 px-4 border border-gray-300 rounded-md focus:outline-none focus:border-blue-500" 
      />
      
      <div className='flex justify-around items-center'>
        <button
          onClick={handleBet}
          disabled={betAmount === 0 || selectedTeam === ""}
          className="py-2 px-4 bg-blue-500 hover:bg-blue-600 text-white rounded-md disabled:bg-gray-400 disabled:pointer-events-none"
        >
          Bet
        </button>
        <p className="">You have {user.coins} coins.</p>
      </div>
      
      {hasBet && <div className='bg-green-500 rounded-lg p-1 text-white text-center'>Bet has been recorded!</div>}
      {hasError && <div className='bg-red-500 rounded-lg p-1 text-white text-center'>{errorMessage}</div>}
    </div>
  );
};

export default BetSelection;
