import React from 'react'
import { CoreStats } from '../types/Match'

interface TeamTableProps {
  core: Partial<CoreStats> | undefined
}

const TableTeam: React.FC<TeamTableProps> = ( props ) => {

  const roundDownIfNeeded = (key: string, value: number) => {
    return key === 'score' ? Math.floor(value) : Math.round(value * 10) / 10
  }
  const filteredEntries = props.core ? Object.entries(props.core).filter(([key]) => key !== 'shootingPercentage') : []

  return (
    <table className="border border-gray-300 shadow-md">
      <tbody className="divide-y divide-gray-300">
        {filteredEntries.length > 0 ? (
          filteredEntries.map(([key, value]) => {
            if (typeof key !== 'string') {
              console.error('Invalid key:', key)
              return null
            }
            return (
              <tr key={key}>
                <td className="px-6 py-4 font-medium text-gray-800">{key}</td>
                <td className="px-6 py-4 text-gray-600">{roundDownIfNeeded(key, value)}</td>
              </tr>
            )
          })
        ) : (
          <tr>
            <td colSpan={2} className="px-6 py-4 text-gray-600">No data available</td>
          </tr>
        )}
      </tbody>
    </table>
  )
}

export default TableTeam;
