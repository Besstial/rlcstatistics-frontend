import { useState } from "react"
import signup from "../functions/signup"

interface RegisterFormProps {
  isActive: boolean
  onShow: () => void
}

const RegisterForm: React.FC<RegisterFormProps> = ( props ) => {

  const [errorSignupMessage, setErrorSignupMessage] = useState("")
  const [email, setEmail] = useState("")
  const [password, setPassword] = useState("")
  const [username, setUsername] = useState("")

  const handleUsernameChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setUsername(event.target.value)
  }

  const handleEmailChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setEmail(event.target.value)
  }

  const handlePasswordChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setPassword(event.target.value)
  }

  const handleSignUpSubmit = async (e: React.FormEvent) => {
    e.preventDefault()
    try {
      const [response, signupMessage] = await signup(email, username, password)
      setErrorSignupMessage(signupMessage)
      response && props.onShow()
    } catch (error) {
      console.error("An error occurred during login:", error)
      setErrorSignupMessage("An error occurred during signup. Please try again.")
    }
  }


  return (
    <div className="flex justify-center">

{
        props.isActive &&
        <div className="p-4 rounded shadow-md">
          <h2 className="text-xl font-bold mb-4">Register</h2>
          <form onSubmit={handleSignUpSubmit}>
            <div className="mb-4">
              <label className="text-gray-700 font-bold" htmlFor="email">Email</label>
              <input
                className="border rounded w-full p-2 text-gray-700 focus:outline-none"
                type="email"
                name="email"
                placeholder="myemail@smtp.com"
                value={email}
                onChange={handleEmailChange}
              />
            </div>
            <div className="mb-4">
              <label className="text-gray-700 font-bold" htmlFor="password">Username</label>
              <input
                className="border rounded w-full p-2 text-gray-700 focus:outline-none"
                type="text"
                name="text"
                placeholder="mybestname"
                value={username}
                onChange={handleUsernameChange}
              />
            </div>
            <div className="mb-4">
              <label className="text-gray-700 font-bold" htmlFor="password">Password</label>
              <input
                className="border rounded w-full p-2 text-gray-700 focus:outline-none"
                type="password"
                name="password"
                placeholder="**********"
                value={password}
                onChange={handlePasswordChange}
              />
            </div>
            <div className="flex items-center justify-between mb-4">
              <button className="bg-green-500 hover:bg-green-700 text-white font-bold py-2 px-4 rounded" type="submit" disabled={!email || !password || !username}>Sign Up</button>
              <button className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded" type="button" onClick={props.onShow}>Back to Login</button>
            </div>
          </form>
          {
            errorSignupMessage && 
            <div className="bg-red-100 border border-red-400 text-red-700 px-4 py-2 rounded" role="alert">
              <strong className="font-bold">{errorSignupMessage}</strong>
            </div>
          }
        </div>
      }
    </div>
      
  )
}

export default RegisterForm;
