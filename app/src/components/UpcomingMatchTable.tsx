import React from "react"
import { useNavigate } from "react-router-dom"
import { UpcomingMatch } from "../types/Match"

export interface UpcomingMatchTableProps {
  upcoming_matches: UpcomingMatch[] 
}
  
const UpcomingMatchTable: React.FC<UpcomingMatchTableProps> = ( props ) => {
  
  const navigate = useNavigate()
  const handleRowClick = (matchId: string) => {
    const matchUrl = `/upcoming_match/${matchId}`
    navigate(matchUrl)
  }
  
  return (
    <table className="table-auto bg-white shadow-lg rounded-lg overflow-hidden">
      <thead>
        <tr className="bg-gray-200 text-gray-600 uppercase text-sm leading-normal">
          <th className="py-3 px-4 text-left">Match</th>
          <th className="py-3 px-4 text-left">Date</th>
        </tr>
      </thead>
      <tbody>
        {props.upcoming_matches.map((match, index) => {
          const formattedDate = new Date(match.start_date).toLocaleString([], { year: 'numeric', month: '2-digit', day: '2-digit', hour: '2-digit', minute: '2-digit'})

          return (
            <tr
              key={match.id_match}
              className={`${index % 2 === 0 ? "" : "bg-gray-100"} hover:bg-gray-300 cursor-pointer`}
              onClick={() => handleRowClick(match.id_match)}
            >
              <td className="py-3 px-4">
                <div className="flex items-center">
                  <div>
                    <p className="font-semibold text-orange-500">{match.team_name_orange}</p>
                    <p className="font-semibold text-blue-500">{match.team_name_blue}</p>
                  </div>
                </div>
              </td>
              <td className="py-3 px-4">{formattedDate}</td>
            </tr>
          )
        })}
      </tbody>
    </table>
  )
}

export default UpcomingMatchTable;