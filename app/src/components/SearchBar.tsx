import React, { useState, useEffect } from 'react';
import axios from 'axios';
import api_url from '../config';

interface SearchBarProps {
  onSearch: (searchType: string, query: string) => void;
}

const SearchBar: React.FC<SearchBarProps> = ({ onSearch }) => {
  const [searchType, setSearchType] = useState('team');
  const [query, setQuery] = useState('');
  const [suggestions, setSuggestions] = useState<string[]>([]);
  const [data, setData] = useState<{ teams: string[], players: string[], events: string[] }>({
    teams: [],
    players: [],
    events: []
  });
  const [selectedIndex, setSelectedIndex] = useState(-1);

  useEffect(() => {
    const fetchData = async () => {
      try {
        const [teamsRes, playersRes, eventsRes] = await Promise.all([
          axios.get(`${api_url}/teams`),
          axios.get(`${api_url}/players`),
          axios.get(`${api_url}/events`)
        ]);

        setData({
          teams: teamsRes.data,
          players: playersRes.data,
          events: eventsRes.data
        });
      } catch (error) {
        console.error('Erreur lors de la récupération des données', error);
      }
    };

    fetchData();
  }, []);

  useEffect(() => {
    if (query.length > 1) {
      const filterData = (data: string[]) => {
        return data.filter((item) =>
          item.toLowerCase().includes(query.toLowerCase())
        );
      };

      switch (searchType) {
        case 'team':
          setSuggestions(filterData(data.teams));
          break;
        case 'player':
          setSuggestions(filterData(data.players));
          break;
        case 'event':
          setSuggestions(filterData(data.events));
          break;
        default:
          setSuggestions([]);
          break;
      }
      setSelectedIndex(-1);
    } else {
      setSuggestions([]);
    }
  }, [query, searchType, data]);

  const handleSearchTypeChange = (e: React.ChangeEvent<HTMLSelectElement>) => {
    setSearchType(e.target.value);
    setQuery('');
    setSuggestions([]);
  };

  const handleInputChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setQuery(e.target.value);
  };

  const handleSearch = () => {
    onSearch(searchType, query);
    setSuggestions([]);
  };

  const handleKeyDown = (e: React.KeyboardEvent<HTMLInputElement>) => {
    if (e.key === 'Enter') {
      if (selectedIndex >= 0 && selectedIndex < suggestions.length) {
        setQuery(suggestions[selectedIndex]);
      } else {
        handleSearch();
      }
      setSuggestions([]);
    } else if (e.key === 'ArrowDown') {
      setSelectedIndex((prevIndex) =>
        prevIndex < suggestions.length - 1 ? prevIndex + 1 : 0
      );
    } else if (e.key === 'ArrowUp') {
      setSelectedIndex((prevIndex) =>
        prevIndex > 0 ? prevIndex - 1 : suggestions.length - 1
      );
    }
  };

  const handleSuggestionClick = (suggestion: string) => {
    setQuery(suggestion);
    setSuggestions([]);
  };

  return (
    <div className="relative">
      <div className="flex items-center border rounded-lg p-2">
        <select
          value={searchType}
          onChange={handleSearchTypeChange}
          className="bg-gray-200 rounded-l-lg p-2 border-r focus:outline-none"
        >
          <option value="team">Team</option>
          <option value="player">Player</option>
          <option value="event">Event</option>
        </select>

        <input
          type="text"
          value={query}
          onChange={handleInputChange}
          onKeyDown={handleKeyDown}
          placeholder="Search..."
          className="flex-grow p-2 focus:outline-none bg-white"
        />

        <button
          onClick={handleSearch}
          className="bg-blue-500 text-white p-2 rounded-r-lg hover:bg-blue-600"
        >
          Search
        </button>
      </div>

      {suggestions.length > 0 && (
        <ul className="absolute bg-white border border-gray-300 rounded-lg mt-1 w-full max-h-48 overflow-y-auto z-10">
          {suggestions.map((suggestion, index) => (
            <li
              key={index}
              onClick={() => handleSuggestionClick(suggestion)}
              className={`p-2 cursor-pointer hover:bg-gray-100 ${
                index === selectedIndex ? 'bg-blue-100' : ''
              }`}
            >
              {suggestion}
            </li>
          ))}
        </ul>
      )}
    </div>
  );
};

export default SearchBar;
