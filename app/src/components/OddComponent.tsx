import React from 'react'

interface OddComponentProps {
    team_name: string
    odd: number
    color: string
}

const OddComponent: React.FC<OddComponentProps> = ( props ) => {

  return (
    <div className={`flex flex-col items-center rounded-lg bg-${props.color}-500 px-2 text-white font-bold`}>
        <p>{props.team_name}</p>
        <p>{props.odd}</p>
    </div>
  )
}

export default OddComponent;