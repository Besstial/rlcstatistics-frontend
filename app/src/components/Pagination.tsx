interface PaginationProps {
    currentPage: number
    pageSize: number
    totalItems: number
    onPageChange: (newPage: number) => void
}

const Pagination: React.FC<PaginationProps> = ({ currentPage, pageSize, totalItems, onPageChange }) => {
    const totalPages = Math.ceil(totalItems / pageSize)

    const handlePageClick = (page: number) => {
        onPageChange(page)
    };

    const displayPageNumbers = () => {
        const pageNumbers = []
        const maxVisiblePages = 3

        if (totalPages <= maxVisiblePages) {
            for (let i = 1; i <= totalPages; i++) {
                pageNumbers.push(i);
            }
        } else {
            const startPage = Math.max(1, currentPage - Math.floor(maxVisiblePages / 2))
            const endPage = Math.min(totalPages, startPage + maxVisiblePages - 1)

            if (startPage > 1) {
                pageNumbers.push(1, '...')
            }

            for (let i = startPage; i <= endPage; i++) {
                pageNumbers.push(i)
            }

            if (endPage < totalPages) {
                pageNumbers.push('...', totalPages)
            }
        }
        return pageNumbers
    };

    return (
        <div className="flex justify-center mt-4">
            {displayPageNumbers().map((page, index) => (
                <button
                    key={index}
                    className={`mx-2 py-2 px-4 ${currentPage === page ? 'bg-blue-500 text-white' : 'bg-gray-200 text-gray-600'} rounded-lg`}
                    onClick={() => (page !== '...' ? handlePageClick(page) : null)}
                    disabled={page === '...'}
                >
                    {page}
                </button>
            ))}
        </div>
    )
}

export default Pagination
