import React from 'react'

const Footer: React.FC = () => {
  return (
<footer className="bg-white py-4">
  <div className="container mx-auto px-4">
    <div className="max-w-screen-lg mx-auto border-t border-gray-300"></div>
    <p className="text-center text-gray-600 text-sm mt-4">&copy; 2024 RLCStatistics</p>
  </div>
</footer>


  )
}

export default Footer;