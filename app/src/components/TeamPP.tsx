import React, { useEffect, useState } from 'react'
import api_url from '../config'

interface TeamPPProps {
  name: string
  color: string
}

const TeamPP: React.FC<TeamPPProps> = ( props ) => {
  const [imageSrc, setImageSrc] = useState<string | null>(null);

  useEffect(() => {
    const fetchImage = async () => {
      try {
        const response = await fetch(`${api_url}/team/img?team_name=${encodeURIComponent(props.name)}`);
        if (response.ok) {
          const blob = await response.blob();
          const imageUrl = URL.createObjectURL(blob);
          setImageSrc(imageUrl);
        } else {
          // Handle error
          console.error(`Error fetching image for team ${props.name}: ${response.status} - ${response.statusText}`);
        }
      } catch (error) {
        console.error(`Error fetching image for team ${props.name}: ${error}`);
      }
    };

    fetchImage();

    // Cleanup when the component unmounts
    return () => {
      if (imageSrc) {
        URL.revokeObjectURL(imageSrc);
      }
    };
  }, [props.name])

  return (
    <div className="flex justify-center">
      <div className="flex flex-col items-center space-y-4">
        {imageSrc && <img src={imageSrc} alt={props.name} className="w-32 h-32 object-cover rounded-lg" />}
        <span className={`text-xl font-semibold text-${props.color}-500`}>{props.name}</span>
      </div>
    </div>
  );
};

export default TeamPP;
