import React from 'react'
import TeamPP from './TeamPP'
import TableTeam from './TableTeam'
import { CoreStats } from '../types/Match'

interface TeamSumUpProps {
  name: string
  core: Partial<CoreStats> | undefined
  color: string
}

const TeamSumUp: React.FC<TeamSumUpProps> = ( props ) => {

  return (
    <div className="space-y-4">
        <TeamPP name={props.name} color={props.color} />
        <TableTeam core={props.core} />
    </div>
  )
}

export default TeamSumUp;
