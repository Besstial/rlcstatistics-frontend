import React from 'react';

interface ScoreProps {
  blue?: number,
  orange?: number
}

const ScoreComponent: React.FC<ScoreProps> = ( props ) => {

  const blue_score = props.blue ? props.blue : 0
  const orange_score = props.orange ? props.orange : 0

  return (
    <div className="flex lg:flex-row justify-center">
        <p className="font-bold text-4xl text-orange-500">{orange_score}</p>
        <p className="font-bold text-4xl px-2"> - </p>
        <p className="font-bold text-4xl text-blue-500">{blue_score}</p>
    </div>
  )
}

export default ScoreComponent;
