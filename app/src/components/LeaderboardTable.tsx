import React from 'react'

interface LeaderboardTableProps {
    list: UserLeaderboard[]
}

const LeaderboardTable: React.FC<LeaderboardTableProps> = ( { list } ) => {
  return (
    <table className="table-auto bg-white shadow-lg rounded-lg overflow-hidden">
      <thead>
        <tr className="bg-gray-200 text-gray-600 uppercase text-sm leading-normal">
          <th className="py-3 px-4 text-left">N°</th>
          <th className="py-3 px-4 text-left">Username</th>
          <th className="py-3 px-4 text-left">Coins</th>
        </tr>
      </thead>
      <tbody>
        {list.map((user, index) => {
          return (
            <tr
              key={user.email}
              className={`${index % 2 === 0 ? "" : "bg-gray-100"} hover:bg-gray-300 cursor-pointer`}
            >
                <td className="py-3 px-4">{index + 1}</td>
                <td className="py-3 px-4">{user.username}</td>
                <td className="py-3 px-4">{user.coins}</td>
            </tr>
          )
        })}
      </tbody>
    </table>
  )
}

export default LeaderboardTable;