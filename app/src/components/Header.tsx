import React from 'react'
import Profile from "../components/Profile"
import { tokenSelector, userState } from "../types/recoil_state.tsx"
import { useRecoilState, useRecoilValue } from 'recoil';
import give_coins from '../functions/give_coins.tsx'
import get_user from '../functions/get_user.tsx';
import { Link } from 'react-router-dom';


function isCurrentDate(targetDate: string) {
  const lastGetCoinsDate = parseInt(targetDate, 10)
  const currentDate = new Date()
  const year = currentDate.getFullYear()
  const month = String(currentDate.getMonth() + 1).padStart(2, '0')
  const day = String(currentDate.getDate()).padStart(2, '0')
  const currentDateInt = parseInt(`${year}${month}${day}`, 10)
  return lastGetCoinsDate < currentDateInt
}
 

const Header: React.FC = () => {
  const [user, setUser] = useRecoilState(userState)
  const token = useRecoilValue(tokenSelector)
  const canRedeem = isCurrentDate(user.refill_date)

  const handleGetCoinsChange = async () => {
    await give_coins(token.token)
    const updatedUser = await get_user(token.token)
    setUser(updatedUser)
  }
  
  return (
    <div className="flex flex-col sm:flex-row items-center justify-around pt-2">
      <header className="flex flex-row items-center p-2">
        <img src="/src/assets/logo-512x512.png" alt="logo" className="w-20 h-20" />
        <Link to="/" className="text-2xl font-bold px-1">RLCStatistics</Link>
      </header>
      <nav className="flex flex-col sm:flex-row items-center">
          {token.isAuthenticated && canRedeem && <button className="text-gray-600 hover:text-gray-800 px-2" onClick={handleGetCoinsChange}>Get coins!</button>}
          <a className="text-gray-600 hover:text-gray-800 px-2" href="/upcoming_match">Upcoming matches</a>
          <a className="text-gray-600 hover:text-gray-800 px-2" href="/search">Search</a>
          <a className="text-gray-600 hover:text-gray-800 px-2" href="/leaderboard">Leaderboard</a>
          {token.isAuthenticated && <a className="text-gray-600 hover:text-gray-800 px-2" href="/bets/me">Bets</a>}
          <Profile />
      </nav>
    </div>
  )
}

export default Header;