import React from "react";
import { useNavigate } from "react-router-dom";
import { IndexMatchTableProps } from "../types/Match";


const IndexMatchTable: React.FC<IndexMatchTableProps> = (props) => {
  const navigate = useNavigate();

  const handleRowClick = (matchId: string) => {
    const matchUrl = `/match/${matchId}`;
    navigate(matchUrl);
  };

  return (
    <table className="table-auto bg-white shadow-lg rounded-lg overflow-hidden w-full">
      <thead>
        <tr className="bg-gray-200 text-gray-600 uppercase text-sm leading-normal">
          <th className="py-3 px-4 text-left">Événement</th>
          <th className="py-3 px-4 text-left">Date</th>
          <th className="py-3 px-4 text-left">Équipe Bleue</th>
          <th className="py-3 px-4 text-left">Équipe Orange</th>
        </tr>
      </thead>
      <tbody>
        {props.matches.map((match, index) => {
          const formattedDate = new Date(match.date).toLocaleString([], {
            year: "numeric",
            month: "2-digit",
            day: "2-digit",
            hour: "2-digit",
            minute: "2-digit",
          });

          return (
            <tr
              key={match.id_match}
              className={`${index % 2 === 0 ? "" : "bg-gray-100"} hover:bg-gray-300 cursor-pointer`}
              onClick={() => handleRowClick(match.id_match)}
            >
              <td className="py-3 px-4">{match.event}</td>
              <td className="py-3 px-4">{formattedDate}</td>
              <td className="py-3 px-4">
                <p className="font-semibold text-blue-500">{match.blue.team}</p>
                <p>{match.blue.players.join(", ")}</p>
              </td>
              <td className="py-3 px-4">
                <p className="font-semibold text-orange-500">{match.orange.team}</p>
                <p>{match.orange.players.join(", ")}</p>
              </td>
            </tr>
          );
        })}
      </tbody>
    </table>
  );
};

export default IndexMatchTable;
