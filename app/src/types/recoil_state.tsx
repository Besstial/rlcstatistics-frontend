import { atom, selector } from "recoil"
import { recoilPersist } from 'recoil-persist'

const { persistAtom } = recoilPersist()

export const userStateInit: User = {
    uuid: "",
    username: "",
    email: "",
    role: "USER",
    creation_date: "",
    refill_date: "",
    coins: 0,
    bets: {}
}

export const tokenStateInit: Token = {
    isAuthenticated: false,
    token: ""
}

export const userState = atom<User>({
    key: 'userState',
    default: userStateInit,
    effects_UNSTABLE: [persistAtom]
})

export const tokenState = atom<Token>({
    key: 'tokenState',
    default: tokenStateInit,
    effects_UNSTABLE: [persistAtom]
})

export const userSelector = selector<User>({
    key: 'userSelector',
    get: ({get}) => {
        const user = get(userState)
        return user
    }
})

export const tokenSelector = selector<Token>({
    key: 'tokenSelector',
    get: ({get}) => {
        const token = get(tokenState)
        return token
    }
})
