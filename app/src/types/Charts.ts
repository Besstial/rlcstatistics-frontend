interface LineChartData {
    date: string
    coins: number
}

interface PieChartData {
    name: string
    value: number
}

interface RenderCustomizedLabelProps {
    cx: number;
    cy: number;
    midAngle: number;
    innerRadius: number;
    outerRadius: number;
    percent: number;
    index: number;
  }