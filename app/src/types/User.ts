interface Token {
    isAuthenticated: boolean
    token: string
}

interface LoginApi {
    token_type: string
    access_token: string
}

interface Bet {
    uuid: string
    date: string
    id_match: string
    amount: number
    team_name: string
    reward: number | null
  }
  
interface User {
  uuid: string
  username: string
  email: string
  role: string
  creation_date: string
  refill_date: string
  coins: number
  bets: Record<string, Bet>
}

interface UserLeaderboard {
  email: string
  username: string
  coins: string
}