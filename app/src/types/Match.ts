export interface CoreStats {
    shots: number
    goals: number
    saves: number
    assists: number
    score: number
}

export const initCoreStats = {
    shots: 0,
    goals: 0,
    saves: 0,
    assists: 0,
    score: 0
}

interface Team {
    team: {
        name: string
    }
    stats: Partial<TeamStats> | undefined
}

interface TeamStats {
    core: Partial<CoreStats> | undefined
}

export interface MatchResult {
    score: number
    winner: boolean
    team: Partial<Team>
}

interface Event {
    name: string
    region: string
    mode: number
    tier: string
}

interface Format {
    length: number
}

export interface MatchData {
    _id: string
    event: Event
    date: string
    format: Format
    blue: MatchResult
    orange: MatchResult
    number: number
}

export type UpcomingMatch = {
    id_match: string
    start_date: string
    team_name_orange: string
    team_name_blue: string
}

export type BetMatch = {
    uuid: string
    date: string
    id_match: string
    amount: number
    team_name: string
    reward?: number
}

export interface TeamDataApi {
    name: string
    score: number
    goals: number
    shots: number
    assists: number
    saves: number
}

export interface UpcomingMatchInfo {
    id_match: string
    blue: TeamDataApi
    orange: TeamDataApi
    blue_odd: number
    orange_odd: number
}

export interface IndexMatch {
    id_match: string;
    date: string;
    event: string;
    blue: {
      team: string;
      players: string[];
    };
    orange: {
      team: string;
      players: string[];
    };
  }
  
  export interface IndexMatchTableProps {
    matches: IndexMatch[];
  }