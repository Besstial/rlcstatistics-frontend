import { createBrowserRouter, Navigate } from "react-router-dom"
import HomePage from "./pages/HomePage"
import ErrorPage from "./pages/ErrorPage"
import HistoryPage from "./pages/HistoryPage"
import NextMatchPage from "./pages/NextMatchPage"
import UpcomingPage from "./pages/UpcomingPage"
import LoginPage from "./pages/LoginPage"
import MatchPage from "./pages/MatchPage"
import LeaderboardPage from "./pages/LeaderboardPage"
import ApodEnsaiPrivacy from "./pages/ApodEnsaiPrivacy"
import SearchPage from "./pages/SearchPage"


const router = createBrowserRouter([
    {
        path: "/",
        Component: HomePage,
        errorElement: <ErrorPage />,
        children: [
            {
                path: "upcoming_match",
                Component: UpcomingPage,
                children: [
                        {
                            path: ":id",
                            Component: NextMatchPage
                        }
                ]
            },
            {
                path: "bets/me",
                Component: HistoryPage,
                children: [
                    {
                        path: "match/:id",
                        Component: MatchPage
                    }
                ]
            },
            {
                path: "match/:id",
                Component: MatchPage
            },
            {
                path: "login",
                Component: LoginPage
            },
            {
                path: "leaderboard",
                Component: LeaderboardPage
            },
            {
                path: "search",
                Component: SearchPage
            },
            {
                path: "/",
                element: <Navigate replace to={`/upcoming_match`} />
            }
        ]
    },
    {
        path: "apodensai/privacy",
        Component: ApodEnsaiPrivacy
    }
])

export default router