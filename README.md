npm create vite@latest RLCStatistics-frontend

npm install react-router-dom localforage match-sorter sort-by
npm install @reduxjs/toolkit react-redux
npm install -D tailwindcss postcss autoprefixer
npx tailwind init -p
npm install axios
npm install redux-persist
npm install reduxjs-toolkit-persist
npm i --save-dev @types/redux-persist
npm install dotenv --save


npm install

npm run dev

sudo service docker start
sudo docker build -t rlcstatistics-frontend:latest .
sudo docker run -p 5173:5173 rlcstatistics-frontend:latest

# dev local run
sudo service docker start
sudo docker-compose -f docker-compose-dev.yml down --remove-orphans
sudo docker-compose -f docker-compose-dev.yml up --build -d
export VITE_ENV=DEV
npm install 
npm run dev